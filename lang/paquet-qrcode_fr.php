<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Q
	'qrcode_description' => 'Ce plugin permet d’utiliser des QRcodes (utilisables par exemple avec votre portable) en particulier en lien avec les documents joints.

Il ajoute aussi : 
-* le modèle << qrcode|texte=...|taille=... >>,
-* le filtre |qrcode
qui permettent de générer des QRcodes aux contenus variés.',
	'qrcode_nom' => 'QrCode',
	'qrcode_slogan' => 'Générateur de code pour une lecture avec téléphone portable ',
);

?>